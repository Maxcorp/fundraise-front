import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        presets: [40, 100, 200, 1000, 2500, 5000],
        priceButtonValues: [40, 100, 200, 1000, 2500, 5000],
        suggestion: 40,
        defaultSymbol: '$',
        currencies: [
            {name: "US Dollar", code: "USD", symbol: "$", rate: 1},
            {name: "Euro", code: "EUR", symbol: "€", rate: 0.897597},
            {name: "British Pound", code: "GBP", symbol: "£", rate: 0.81755},
            {name: "Russian Ruble", code: "RUB", symbol: "₽", rate: 63.461993}
        ],
    },

    getters: {
        getPriceButtonValues: state => {
            state.priceButtonValues = [...state.presets];
            return state.priceButtonValues;
        },
        getCurrencyData: state => selectedCurrency => {
            return state.currencies.find(currency => currency.code === selectedCurrency);
        },
    },

    mutations: {
        setSymbol(state, symbol) {
            state.defaultSymbol = symbol;
        },
        setDonate(state, donate) {
            state.suggestion = donate;

        },
        setPresets(state, presets) {
            state.priceButtonValues = presets;
        },
        updateSuggestion(state, suggestion) {
            state.suggestion = suggestion;
        }
    },

    actions: {
        setSymbol(context, symbol) {
            context.commit('setSymbol', symbol);
        },
        setDonate(context, donate) {
            context.commit('setDonate', donate);
        },
        setPresets(context, presets) {
            context.commit('setPresets', presets);
        },
        updateSuggestion(context, suggestion) {
            context.commit('updateSuggestion', suggestion);
        }
    }
});
