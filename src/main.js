import Vue from 'vue'
import App from './App.vue'
import store from './store/store'
import axios from 'axios'
Vue.prototype.$http = axios;

new Vue({
  store,
  el: '#app',
  render: h => h(App)
})
